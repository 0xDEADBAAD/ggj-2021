using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateMovementAnimation : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private EntityMovement _movement;

    private void Update()
    {
        _animator.SetFloat("Speed", _movement.Direction.sqrMagnitude);

        _animator.SetFloat("HorizontalSpeed", _movement.Direction.x);
        _animator.SetFloat("VerticalSpeed", _movement.Direction.y);
    }
}
