using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class EntityMovement : MonoBehaviour
{
    [SerializeField, Range(0, 10)] private float _moveSpeed;

    public Vector2 Direction => _direction;

    private Vector2 _direction;
    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void SetDirection(Vector2 newDirection)
    {
        _direction = newDirection.normalized;
    }

    private void FixedUpdate()
    {
        _rigidbody.MovePosition(_rigidbody.position + _direction * _moveSpeed * Time.fixedDeltaTime);
    }
}
