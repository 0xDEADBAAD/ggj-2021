using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSensor : MonoBehaviour
{
    public Item NearItem => _nearItem;
    public HidingSpot NearHidingSpot => _activeHidingSpot;
    public ItemHolder NearHolder => _nearItemHolder;

    [SerializeField] private Item _nearItem;    
    [SerializeField] private HidingSpot _activeHidingSpot;
    [SerializeField] private ItemHolder _nearItemHolder;

    public void OnItemPickedUp()
    {
        _nearItem = null;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        _nearItem = other.GetComponentInChildren<Item>();
        _activeHidingSpot = other.GetComponent<HidingSpot>();
        _nearItemHolder = other.GetComponent<ItemHolder>();
    }
    
    private void OnTriggerExit2D(Collider2D other)
    {
        var item = other.GetComponent<Item>();

        if (item != null)
        {
            _nearItem = null;
            return;
        }

        var hidingSpot = other.GetComponent<HidingSpot>();

        if (hidingSpot != null)
        {
            _activeHidingSpot = null;
            return;
        }

        var holder = other.GetComponent<ItemHolder>();

        if (holder != null)
        {
            _nearItemHolder = null;
            return;
        }
    }
}
