using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using UnityEditorInternal;
using UnityEngine;

public enum HoomanState { Idle, LookingForItem, GoingToHidingSpot, LeavingHome }

public class AIPlanner : MonoBehaviour
{
    [SerializeField] private PathfindingProxy _pathfinding;
    [SerializeField] private EntityMovement _movement;
    [SerializeField] private AIBlackboard _blackboard;
    [SerializeField] private float _searchTime;

    [SerializeField] private Item _currentTargetItem;
    [SerializeField] private HidingSpot _currentTargetHidingSpot;
    [SerializeField] private HoomanState _state;
    [SerializeField] private List<Item> _itemsNeeded;
    [SerializeField] private ItemSensor _itemSensor;
    [SerializeField] private List<HidingSpot> _hidingSpotsLeft;

    private void Start()
    {
        var gameManager = FindObjectOfType<GameManager>();
        var rng = new System.Random();       
        var shuffledItems = _blackboard.Items.OrderBy(i => rng.Next()).ToList();

       _itemsNeeded = new List<Item>(Mathf.Min(shuffledItems.Count, gameManager.ItemsToGet));

        for (int i = 0; i < _itemsNeeded.Capacity; i++)
        {
            _itemsNeeded.Add(shuffledItems[i]);
        }

        SetupNextTarget();
    }

    private void OnTargetReached()
    {
        _movement.SetDirection(Vector2.zero);

        var previousState = _state;
        _state = HoomanState.Idle;

        StopMoving();

        if (previousState == HoomanState.LookingForItem)
        {
            if (_itemSensor.NearHolder.HasItem)
            {
                Invoke(nameof(PickupItem), _searchTime / 4);
            }
            else
            {
                Debug.Log("Item is lost!");
                Invoke(nameof(GoToClosestHidingSpot), _searchTime / 2);
            }
        }
        else if (previousState == HoomanState.GoingToHidingSpot)
        {
            if (_currentTargetHidingSpot.HasItem)
            {
                _currentTargetItem = _currentTargetHidingSpot.GetItem();
                
                Invoke(nameof(PickupItem), _searchTime);
            }
            else
            {
                Invoke(nameof(GoToClosestHidingSpot), _searchTime);
            }
        }        
    }

    private void PickupItem()
    {
        Debug.Log($"Item picked up by Hooman: { _currentTargetItem }");

        _blackboard.Items.Remove(_currentTargetItem);
        _itemsNeeded.Remove(_currentTargetItem);
        Destroy(_currentTargetItem.gameObject);

        _hidingSpotsLeft.Clear();

        if (_itemsNeeded.Count == 0)
        {
            Debug.Log("Hooman will be gone D:");
            return;
        }

        SetupNextTarget();
    }
    
    private void SetupNextTarget()
    {
        if (_itemsNeeded.Count > 1)
        {
            _itemsNeeded.Sort((x, y) =>
            {
                var distToX = Vector3.Distance(x.transform.position, transform.position);
                var distToY = Vector3.Distance(y.transform.position, transform.position);

                return distToX <= distToY ? -1 : 1;
            }); 
        }

        _currentTargetItem = _itemsNeeded[0];
        _state = HoomanState.LookingForItem;
        _pathfinding.SetTarget(_currentTargetItem.Holder.transform, OnTargetReached);
    }

    private void GoToClosestHidingSpot()
    {

        if (_hidingSpotsLeft.Count == 0)
        {
            _hidingSpotsLeft.AddRange(_blackboard.HidingSpots);
        }
        else if (_currentTargetHidingSpot != null)
        {
            _hidingSpotsLeft.Remove(_currentTargetHidingSpot);
        }

        _hidingSpotsLeft.Sort((x, y) =>
        {
            var distToX = Vector3.Distance(x.transform.position, transform.position);
            var distToY = Vector3.Distance(y.transform.position, transform.position);

            return distToX <= distToY ? -1 : 1;
        });

        if (_hidingSpotsLeft.Count == 0)
        {
            StopMoving();
            GoToClosestHidingSpot();
            
            // TODO: implement timeout

            return;
        }

        _state = HoomanState.GoingToHidingSpot;
        _currentTargetHidingSpot = _hidingSpotsLeft[0];

        _pathfinding.SetTarget(_currentTargetHidingSpot.transform, OnTargetReached);
    }

    [Button]
    private void StopMoving()
    {
        _pathfinding.StopMoving();
        _movement.SetDirection(Vector2.zero);
    }

    [Button]
    private void StartMoving()
    {
        _pathfinding.StartMoving();
        
        if (_state == HoomanState.LookingForItem)
        {
            _pathfinding.SetTarget(_currentTargetItem.Holder.transform, OnTargetReached);
        }
        else if (_state == HoomanState.GoingToHidingSpot)
        {
            _pathfinding.SetTarget(_currentTargetHidingSpot.transform, OnTargetReached);
        }
    }
}
