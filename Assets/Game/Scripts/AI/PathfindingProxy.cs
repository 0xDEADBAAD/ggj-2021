using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using SAP2D;
using UnityEngine;

public class PathfindingProxy : MonoBehaviour
{
    [SerializeField] private SAP2DAgent _agent;
    private bool IsMoving => _agent.isMoving;
    
    private Action _reachCallback;

    public void SetTarget(Transform t, Action onReach)
    {
        _agent.CanSearch = true;
        _agent.CanMove = true;

        _agent.Target = t;
        
        _reachCallback = onReach;

        Debug.Log("AL:SKFJAL:KSJF");
    }
    
    private void Update()
    {
        if (_agent.Target == null) return;

        var distance = Vector3.Distance(transform.position, _agent.Target.position);

        //Debug.Log(distance);

        if (distance < 0.5f)
        {
            _reachCallback?.Invoke();
             //Stop();
        }
    }

    public void StartMoving()
    {
        _agent.CanMove = true;
    }

    public void StopMoving()
    {
        _agent.CanMove = false;
        _agent.CanSearch = false;
        _agent.path = null;
        _agent.Target = null;
        _reachCallback = null;
    }
}
