using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBlackboard : MonoBehaviour
{
    public List<Item> Items = new List<Item>();
    public List<HidingSpot> HidingSpots = new List<HidingSpot>();

    private void Awake()
    {
        Items.AddRange(FindObjectsOfType<Item>());
        HidingSpots.AddRange(FindObjectsOfType<HidingSpot>());
    }
}
