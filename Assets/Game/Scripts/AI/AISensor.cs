using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

public class AISensor : MonoBehaviour
{
    [SerializeField] private LayerMask _visibleObjects;
    [SerializeField] private UnityEvent<GameObject> _onSomethingDetected;
    [SerializeField, Tag] private string[] _triggerTags;

    [SerializeField] private List<Transform> _objectsOnRange = new List<Transform>();

    private Vector3 _rayOrigin;
    private Vector3 _rayDirection;

    private void OnTriggerEnter2D(Collider2D other)
    {
        _objectsOnRange.Add(other.transform);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _objectsOnRange.Remove(other.transform);
    }

    private void FixedUpdate()
    {
        foreach (var element in _objectsOnRange)
        {
            _rayOrigin = transform.position;
            _rayDirection = element.position - transform.position;

            var hit = Physics2D.Raycast(_rayOrigin, _rayDirection, Mathf.Infinity, _visibleObjects);

            if (hit && hit.collider.transform.parent == element.transform.parent)
            {
                Debug.DrawRay(_rayOrigin, (Vector3)hit.point - _rayOrigin, Color.green);
                _onSomethingDetected.Invoke(hit.collider?.gameObject);
            }
        }
    }

    private bool ObjectHasTriggerTag(GameObject other)
    {
        return _triggerTags.Contains(other.tag);
    }
}
