using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.InputSystem;

public class ItemHandler : MonoBehaviour
{
    [SerializeField] private ItemSensor _itemSensor;
    [SerializeField] private Transform _itemAnchor;
    [SerializeField] private Item _heldItem;
    
    private bool IsHoldingItem => _heldItem != null;
    private bool IsCloseToHidingSpot => _itemSensor.NearHidingSpot != null;
    private bool IsCloseToItem => _itemSensor.NearItem != null;
    private bool CanPickupItem => !IsHoldingItem && IsCloseToItem;
    private bool CanHideItem => IsHoldingItem && IsCloseToHidingSpot && !_itemSensor.NearHidingSpot.HasItem;

    [Button]
    private void HideItem()
    {
        if (!CanHideItem) return;

        _itemSensor.NearHidingSpot.HideItem(_heldItem);

        _heldItem = null;
    }

    [Button]
    private void  PickupItem()
    {
        if (!CanPickupItem) return;

        _heldItem = _itemSensor.NearItem;

        _heldItem.transform.parent = _itemAnchor;
        _heldItem.transform.localPosition = Vector3.zero;

        _itemSensor.OnItemPickedUp();
    }

    

    private void OnInteraction(InputValue v)
    {
        if (CanHideItem)
        {
            HideItem();
            return;
        }

        if (CanPickupItem)
        {
            PickupItem();
        }
    }
}
