using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class MovementDebug : MonoBehaviour
{
    private EntityMovement _movement;

    [SerializeField] private Vector2 _direction;

    private void Awake()
    {
        _movement = GetComponent<EntityMovement>();
    }

    [Button]
    private void SetMovement()
    {
        _movement.SetDirection(_direction);
    }
}
