using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterMovement : MonoBehaviour
{   
    private EntityMovement _movement;

    private void Awake()
    {
        _movement = GetComponent<EntityMovement>();
    }

    private void OnMovement(InputValue input)
    {
        _movement.SetDirection(input.Get<Vector2>());
    }
}
