using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingSpot : MonoBehaviour
{
    [SerializeField] private Item _hiddenItem;

    public bool HasItem => _hiddenItem != null;

    public void HideItem(Item i)
    {
        _hiddenItem = i;

        _hiddenItem.transform.parent = transform;
        _hiddenItem.transform.localPosition = Vector3.zero;

        _hiddenItem.Hide();
    }

    public Item GetItem()
    {
        var item = _hiddenItem;
        _hiddenItem = null;

        return item;
    }
}
