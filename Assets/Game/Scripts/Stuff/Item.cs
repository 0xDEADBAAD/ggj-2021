using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public Sprite Sprite => _renderer.sprite;
    private SpriteRenderer _renderer;

    public ItemHolder Holder { get; private set; }

    private void Awake()
    {
        _renderer = GetComponentInChildren<SpriteRenderer>();
        
        Holder = transform.parent.GetComponent<ItemHolder>();
    }

    public void Hide()
    {
        _renderer.enabled = false;
    }

    public void Show()
    {
        _renderer.enabled = true;
    }
}
