using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHolder : MonoBehaviour
{
    public bool HasItem => transform.childCount > 0;

    private void Awake()
    {
        gameObject.name = $"{ transform.GetChild(0).gameObject.name }_Holder";
    }
}
