using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Range(1, 10)] public int ItemsToGet = 1;
    [Range(30, 300)] public float TimeLeft = 240;

    private void Update()
    {
        TimeLeft -= Time.deltaTime;

        if (TimeLeft < 0)
        {
            // TODO: implement timeout
        }
    }
}
